# 📒 감정 온도 일기장, **Deer Diary**

힘든 하루를 보낸 오늘의 나,
<br/>자랑하고 싶은 좋은 일이 생겼던 날,
<br/>누구에게도 터놓을 수 없는 고민,

모두 Deer Diary에 적어 보아요.



## 나에 대해 알아보는 시간
Deer Diary는 Android Platform을 기반으로 만들어진 나의 일기를 바탕으로 한 **머신러닝 기반 감정온도 Mobile Application** 입니다.

현재는 아쉽게도 Android OS만 지원하고 있어요.
<br/>(추후 공개될 iOS 버전도 많이 기대해주세요)

- <a href="https://developer.android.com" target="_blank"><img src="https://img.shields.io/badge/Android-3DDC84?style=flat-square&logo=Android&logoColor=white"/></a> <a href="https://kotlinlang.org" target="_blank"><img src="https://img.shields.io/badge/Kotlin-7F52FF?style=flat-square&logo=Kotlin&logoColor=white"/></a> <a href="https://www.java.com/" target="_blank"><img src="https://img.shields.io/badge/Java-007396?style=flat-square&logo=Java&logoColor=white"/></a>

- ≥ Android 6.0 (Marshmallow)
- 인터넷 연결 필요

## 기능 소개
### 🤔 나조차도 알 수 없는 마음, Deer Diary가 알려줄게요.


### [ 핵심 기능 ]

#### 1. 다른 사람들의 일기를 살펴보아요.
💬 **일상 속 다양한 감정을 나눠요.**
<br/>나와 감정 온도가 비슷한 사람들의 일기를 추천해 보여줍니다.

![image](/uploads/ef0459ce7da5eab6235543e904adca32/image.png)

#### 2. 오늘의 감정일기를 작성해보아요.
📃 오늘 나의 하루를 🦌**Deer**에게 알려주세요.

![image](/uploads/f8ab731838c7cd9d099ad3beb273d4ed/image.png)


#### 3. 나의 오늘 감정을 객관적으로 살펴보아요.

❤️ 내가 작성한 일기의 내용을 토대로 Machine Learning(RNN)을 통해 **그 날의 감정을 분석해 알려줘요.**

![image](/uploads/4ef04be9c8ab746cbf2ac2ec7260d868/image.png)
![image](/uploads/63df94fdc3bc4ba354a22eeb70ffe72e/image.png)


#### 4. 퀘렌시아(Querencia) Tip
나의 마음에도 휴식이 필요하니까.

🚨 마음이 불안할 때,
<br/>🌄 편안하고 즐겁게 만드는 Tip들을 알려줄게요.

- 온전한 힐링을 즐길 수 있는 **Deer 심리테라피**
- 마음에 위안이 될 수 있는 **명상 ASMR**
- 나의 마음에 **안정을 가져다 줄 물건과 장소**를 소개해요.
- (예정) 지금 바로 공감과 위안이 필요할 때 예약없이 바로상담 가능한 상담사를 소개시켜드려요.



![image](/uploads/489aa73e1a4094df1f31f2915f877961/image.png)
![image](/uploads/aad4e49aebb061fad8d002b2fad4e2a6/image.png)
![image](/uploads/125a4b61665bec525c550b0094351f02/image.png)


### [ 기타 ]

#### 로그인/회원가입
![image](/uploads/540cd4a2c3dcce63d18ea941b8a8d9b5/image.png)
![image](/uploads/a1a5e47d3aaf72775e15ccc250d9fb54/image.png)
![image](/uploads/f18c4144747152f8b5093530ec343f2f/image.png)

#### 내 프로필
![image](/uploads/d6ee57db626e9d75c6ddbaeeed3353f4/image.png)


## Application Structure
![Application Architecture](/uploads/7c37ecc09b5db473badcb36af45163a9/image.png)

## Android Build Caution
1. You should open 'DeerDiary' directory not 'module'
2. Create and set your own 'local.properties' file on 'DeerDiary' directory

## Contributing

- Deer Diary is a free and open source project developed by volunteers.
- Any contributions are welcome. Here are a few ways you can help: Report bugs and make suggestions.
- Write some code. Please follow the code style used in the project to make a review process faster.


### License
- This application is released under GNU GPLv3 (see LICENSE).
- Some of the used libraries are released under different licenses.

### Info
본 프로젝트는 '2022 슈퍼챌린지 소프트웨어 해커톤' 희초리 참가팀의 작업물입니다.
<br/>앱이 마음에 든다면, 힘이 될 수 있게 프로젝트의 🌟STAR를 눌러주세요-!


This project is the work of the participating '희초리' in '2022 Super Challenge Software Hackathon'.
<br/>If you like this app, plz press project 🌟STAR :)


@jenny124, @omk2477, @eun7jii3, @RealKunse, @magmacoffees
