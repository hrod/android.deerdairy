package com.hrod.deerdiary.adpater

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.hrod.deerdiary.R

class SelectDiaryAdapter(var context: Context, var list: ArrayList<String>): BaseAdapter() {

    var selectedPosition: Int = 0

    override fun getCount(): Int {
        return list.size
    }

    override fun getItem(position: Int): String {
        return list[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        var result = convertView

        if(result == null){
            result = LayoutInflater.from(context).inflate(R.layout.item_select_diary, parent, false)
        }
        val diaryName = result!!.findViewById<TextView>(R.id.write_diary_name)
        diaryName.text = getItem(position)

        diaryName.setOnClickListener {
            selectedPosition = position
        }

        return result
    }
}