package com.hrod.deerdiary.adpater

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.marginBottom
import com.hrod.deerdiary.R
import com.hrod.deerdiary.model.Emotion

class EmotionAdapter(var context: Context?, var list: ArrayList<Emotion>): BaseAdapter() {

    override fun getCount(): Int {
        return list.size
    }

    override fun getItem(position: Int): Emotion {
        return list[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        var result = convertView
        val emotion = getItem(position)

        if(result == null){
            result = LayoutInflater.from(context).inflate(R.layout.item_last_week_emotion, parent, false)
        }

        val emotionImg = result!!.findViewById<ImageView>(R.id.emotion_img)
        val emotionState = result.findViewById<TextView>(R.id.emotion_state)

        emotionState.text = emotion.state

        return result
    }
}