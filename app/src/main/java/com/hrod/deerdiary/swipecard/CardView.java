package com.hrod.deerdiary.swipecard;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

/**
 * Diary Single CardView
 */

public class CardView extends LinearLayout {

    private View mMyView;

    public CardView(Context context) {
        super(context);
    }

    public CardView(Context context, int layoutId) {
        super(context);
        inflateLayout(context, layoutId);
    }
    private void inflateLayout(Context context, int layoutId) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        mMyView = inflater.inflate(layoutId, this, true);
    }
    public View getMyView() {
        return mMyView;
    }
}