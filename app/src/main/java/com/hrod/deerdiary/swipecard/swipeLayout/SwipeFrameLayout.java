package com.hrod.deerdiary.swipecard.swipeLayout;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;


import com.hrod.deerdiary.swipecard.SwipeCard;

import java.util.ArrayList;

/**
 * SwipeFrameLayout
 */
public class SwipeFrameLayout extends FrameLayout {
    public SwipeFrameLayout(Context context) {
        super(context);
        setClipChildren(false);
    }

    public SwipeFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        setClipChildren(false);
    }

    public SwipeFrameLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setClipChildren(false);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public SwipeFrameLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setClipChildren(false);
    }

    //this is so that on versions of android pre lollipop it will render the cardStack above
    //everything else within the layout
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        int childCount = getChildCount();
        ViewGroup.LayoutParams params = getLayoutParams();
//        ViewGroup.MarginLayoutParams params2 = (MarginLayoutParams) getLayoutParams();
//        params2.setMargins(0,0,50,0);

        ArrayList<View> children = new ArrayList<>();
        View swipeDeck = null;
        for(int i=0; i< childCount; ++i){
            View child = getChildAt(i);
            if(child instanceof SwipeCard){
                swipeDeck = getChildAt(i);
            }else{
                children.add(child);
            }
        }
        removeAllViews();
        removeAllViewsInLayout();
        for(View v : children){
            addViewInLayout(v, -1, v.getLayoutParams(), true);
        }
        if(swipeDeck != null){
            addViewInLayout(swipeDeck, -1, swipeDeck.getLayoutParams(), true);
        }
        invalidate();
        requestLayout();
    }

}
