package com.hrod.deerdiary.spotlight;

/**
 * OnSpotlightEndedListener
 **/
public interface OnSpotlightEndedListener {
    /**
     * 끝날때 호출!!
     */
    void onEnded();
}
