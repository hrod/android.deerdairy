package com.hrod.deerdiary.spotlight;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


import com.hrod.deerdiary.R;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by heejae on 2018-04-12.
 * Spotlight
 **/
public class Spotlight {

    private static final long START_SPOTLIGHT_DURATION = 500L;
    private static final long FINISH_SPOTLIGHT_DURATION = 500L;
    private static final long DEFAULT_DURATION = 1000L;

    @ColorInt
    private static final int DEFAULT_OVERLAY_COLOR = R.color.bg_default;

    private static final TimeInterpolator DEFAULT_ANIMATION = new DecelerateInterpolator(1.5f);

    private static WeakReference<SpotlightView> spotlightViewWeakReference;
    private static WeakReference<Activity> contextWeakReference;
    private ArrayList<? extends Target> targets;
    private ArrayList<Boolean> enableOnTouchedOutside;
    private ArrayList<Boolean> enableOnTouchedInside;

    private long duration = DEFAULT_DURATION;
    private TimeInterpolator animation = DEFAULT_ANIMATION;
    private OnSpotlightStartedListener startedListener;
    private OnSpotlightEndedListener endedListener;
    private int overlayColor = DEFAULT_OVERLAY_COLOR;
    private boolean isClosedOnTouchedOutside = true;
    private boolean isClosedOnTouchedInside = true;
    private boolean isAllClosedOnTouchedOutside = false;
    private boolean isAllClosedOnTouchedInside = false;

    private Spotlight(Activity activity) {
        contextWeakReference = new WeakReference<>(activity);
    }

    public static Spotlight with(@NonNull Activity activity) {
        return new Spotlight(activity);
    }

    private static Context getContext() {
        return contextWeakReference.get();
    }

    @Nullable
    private static SpotlightView getSpotlightView() {
        return spotlightViewWeakReference.get();
    }

    public <T extends Target> Spotlight setTargets(@NonNull T... targets) {
        this.targets = new ArrayList<>(Arrays.asList(targets));
        for (final Target target : targets) {
            if (target instanceof DeerDiaryTarget) {
                ((DeerDiaryTarget) target).setOnTargetActionListener(new DeerDiaryTarget.OnTargetActionListener() {
                    @Override
                    public void closeRequested() {
                        finishTarget();
                    }
                });
            }
        }
        return this;
    }

    public Spotlight setOverlayColor(@ColorInt int overlayColor) {
        this.overlayColor = overlayColor;
        return this;
    }

    public Spotlight setDuration(long duration) {
        this.duration = duration;
        return this;
    }

    public Spotlight setAnimation(TimeInterpolator animation) {
        this.animation = animation;
        return this;
    }

    public Spotlight setOnSpotlightStartedListener(
            @NonNull final OnSpotlightStartedListener listener) {
        startedListener = listener;
        return this;
    }

    public Spotlight setOnSpotlightEndedListener(@NonNull final OnSpotlightEndedListener listener) {
        endedListener = listener;
        return this;
    }

    public Spotlight setClosedOnTouchedDisable(){
        this.isClosedOnTouchedOutside = false;
        this.isClosedOnTouchedInside = false;
        return this;
    }

    public Spotlight setClosedOnTouchedEnable(){
        this.isClosedOnTouchedOutside = true;
        this.isClosedOnTouchedInside = true;
        return this;
    }

    public Spotlight setClosedOnTouchedOutside(boolean isClosedOnTouchedOutside) {
        this.isClosedOnTouchedOutside = isClosedOnTouchedOutside;
        return this;
    }

    public Spotlight setClosedOnTouchedInside(boolean isClosedOnTouchedInside) {
        this.isClosedOnTouchedInside = isClosedOnTouchedInside;
        return this;
    }

    public void start() {
        spotlightView();
    }

    @SuppressWarnings("unchecked")
    private void spotlightView() {
        if (getContext() == null) {
            throw new RuntimeException("context is null");
        }
        final View decorView = ((Activity) getContext()).getWindow().getDecorView();
        SpotlightView spotlightView = new SpotlightView(getContext());
        spotlightViewWeakReference = new WeakReference<>(spotlightView);
        spotlightView.setOverlayColor(overlayColor);
        spotlightView.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        ((ViewGroup) decorView).addView(spotlightView);
        spotlightView.setOnSpotlightStateChangedListener(new SpotlightView.OnSpotlightStateChangedListener() {
            @Override
            public void onTargetClosed() {
                if (!targets.isEmpty()) {
                    Target target = targets.remove(0);
                    if (target.getListener() != null) target.getListener().onEnded(target);
                    if (targets.size() > 0) {
                        startTarget();
                    } else {
                        finishSpotlight();
                    }
                }
            }

            @Override
            public void onTargetClickedOutside() {
                if (isClosedOnTouchedOutside) {
                    finishTarget();
                }
            }

            @Override
            public void onTargetClickedInside() {
                if (isClosedOnTouchedInside) {
                    finishTarget();
                }
            }
        });
        startSpotlight();
    }

    @SuppressWarnings("unchecked")
    private void startTarget() {
        if (targets != null && targets.size() > 0 && getSpotlightView() != null) {
            Target target = targets.get(0);
            getSpotlightView().removeAllViews();
            getSpotlightView().addView(target.getView());
            getSpotlightView().turnUp(target.getPoint().x, target.getPoint().y, target.getRadius(),
                    duration, animation);
            if (target.getListener() != null) target.getListener().onStarted(target);
        }
    }

    private void startSpotlight() {
        if (getSpotlightView() == null) return;
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(getSpotlightView(), "alpha", 0f, 1f);
        objectAnimator.setDuration(START_SPOTLIGHT_DURATION);
        objectAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                if (startedListener != null) startedListener.onStarted();
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                startTarget();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        objectAnimator.start();
    }

    private void finishTarget() {
        if (targets != null && targets.size() > 0 && getSpotlightView() != null) {
            Target target = targets.get(0);
            getSpotlightView().turnDown(target.getRadius(), duration, animation);
        }
    }

    private void finishSpotlight() {
        if (getSpotlightView() == null) return;
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(getSpotlightView(), "alpha", 1f, 0f);
        objectAnimator.setDuration(FINISH_SPOTLIGHT_DURATION);
        objectAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                final View decorView = ((Activity) getContext()).getWindow().getDecorView();
                ((ViewGroup) decorView).removeView(getSpotlightView());
                if (endedListener != null) endedListener.onEnded();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        objectAnimator.start();
    }

}
