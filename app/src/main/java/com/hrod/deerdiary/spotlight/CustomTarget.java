package com.hrod.deerdiary.spotlight;

import android.app.Activity;
import android.graphics.PointF;
import android.os.Handler;
import android.view.View;

import androidx.annotation.LayoutRes;

/**
 * todo: OnTouched 추가하기
 */
public class CustomTarget implements Target {

    private PointF point;
    private float radius;
    private View view;
    private OnTargetStateChangedListener stateListener;
    private OnTargetActionListener actionListener;

    private CustomTarget(PointF point, float radius, View view, OnTargetStateChangedListener listener) {
        this.point = point;
        this.radius = radius;
        this.view = view;
        this.stateListener = listener;
    }

    interface OnTargetActionListener {
        void closeRequested();
    }

    void setOnTargetActionListener(OnTargetActionListener listener) {
        this.actionListener = listener;
    }

    public void closeTarget() {
        actionListener.closeRequested();
    }

    public void closeTarget(long delayTime){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                actionListener.closeRequested();
            }
        }, delayTime);
    }

    @Override
    public PointF getPoint() {
        return point;
    }

    @Override
    public float getRadius() {
        return radius;
    }

    @Override
    public View getView() {
        return view;
    }

    @Override
    public OnTargetStateChangedListener getListener() {
        return stateListener;
    }

    public static class Builder extends AbstractBuilder<Builder, CustomTarget> {

        @Override
        protected Builder self() {
            return this;
        }

        private View view;

        private static final int ABOVE_SPOTLIGHT = 0;
        private static final int BELOW_SPOTLIGHT = 1;

        private CharSequence title = null;
        private CharSequence description = null;

        public Builder(Activity context) {
            super(context);
        }

        public Builder setTitle(CharSequence title) {
            this.title = title;
            return this;
        }

        public Builder setDescription(CharSequence description) {
            this.description = description;
            return this;
        }

        public Builder setView(@LayoutRes int layoutId) {
            if (getContext() == null) {
                throw new RuntimeException("context is null");
            }
            this.view = getContext().getLayoutInflater().inflate(layoutId, null);
            return this;
        }

        public Builder setView(View view) {
            this.view = view;
            return this;
        }

        @Override
        public CustomTarget build() {
            PointF point = new PointF(startX, startY);
            return new CustomTarget(point, radius, view, listener);
        }
    }
}
