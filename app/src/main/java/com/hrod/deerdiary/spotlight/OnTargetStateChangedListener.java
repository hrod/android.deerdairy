package com.hrod.deerdiary.spotlight;

/**
 * OnTargetStateChangedListener
 **/
public interface OnTargetStateChangedListener<T extends Target> {
    /**
     * Target started 되었으면 호출
     */
     void onStarted(T target);

    /**
     * Target ended 되었으면 호출
     */
    void onEnded(T target);
}
