package com.hrod.deerdiary.spotlight;

/**
 * OnSpotlightStartedListener
 **/
public interface OnSpotlightStartedListener {
    /**
     * 시작할 때 호출!!
     */
    void onStarted();
}
