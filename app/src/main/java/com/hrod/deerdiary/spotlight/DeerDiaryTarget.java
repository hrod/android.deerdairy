package com.hrod.deerdiary.spotlight;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.graphics.PointF;
import android.os.Handler;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.hrod.deerdiary.R;


/**
 *
 * todo: OnTouched 추가하기
 */
public class DeerDiaryTarget implements Target {

    private PointF point;
    private float radius;
    private View view;
    private OnTargetStateChangedListener stateListener;
    private OnTargetActionListener actionListener;

    private DeerDiaryTarget(PointF point, float radius, View view, OnTargetStateChangedListener listener) {
        this.point = point;
        this.radius = radius;
        this.view = view;
        this.stateListener = listener;
    }

    interface OnTargetActionListener {
        void closeRequested();
    }

    void setOnTargetActionListener(OnTargetActionListener listener) {
        this.actionListener = listener;
    }

    public void closeTarget() {
        actionListener.closeRequested();
    }

    public void closeTarget(long delayTime){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                actionListener.closeRequested();
            }
        }, delayTime);
    }

    @Override
    public PointF getPoint() {
        return point;
    }

    @Override
    public float getRadius() {
        return radius;
    }

    @Override
    public View getView() {
        return view;
    }

    @Override
    public OnTargetStateChangedListener getListener() {
        return stateListener;
    }

    public static class Builder extends AbstractBuilder<Builder, DeerDiaryTarget> {

        @Override
        protected Builder self() {
            return this;
        }

        public static final int ABOVE_SPOTLIGHT = 0x00000000;
        public static final int BELOW_SPOTLIGHT = 0x00000001;
        private static boolean forceLocation = false;
        private static int forceLocationInt = -1;
        private CharSequence description = null;


        public Builder(Activity context) {
            super(context);
        }

        public Builder setDescription(CharSequence description) {
            this.description = description;
            return this;
        }
        public Builder setLocation(int i){
            this.forceLocation = true;
            this.forceLocationInt = i;
            return this;
        }

        @Override
        public DeerDiaryTarget build() {
            if (getContext() == null) {
                throw new RuntimeException("context is null");
            }
            View view = getContext().getLayoutInflater().inflate(R.layout.layout_spotlight, null);
            ((TextView) view.findViewById(R.id.description)).setText(description);
            PointF point = new PointF(startX, startY);
            calculatePosition(point, radius, view);
            return new DeerDiaryTarget(point, radius, view, listener);
        }

        private void calculatePosition(final PointF point, final float radius, View spotlightView) {
            float[] areas = new float[2];
            Point screenSize = new Point();
            ((WindowManager) spotlightView.getContext()
                    .getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getSize(screenSize);

            areas[ABOVE_SPOTLIGHT] = point.y / screenSize.y;
            areas[BELOW_SPOTLIGHT] = (screenSize.y - point.y) / screenSize.y;

            int largest;
            if (areas[ABOVE_SPOTLIGHT] > areas[BELOW_SPOTLIGHT]) {
                largest = ABOVE_SPOTLIGHT;
            } else {
                largest = BELOW_SPOTLIGHT;
            }

            // fix value
            if(!forceLocation) largest = BELOW_SPOTLIGHT;
            else largest = forceLocationInt;
            forceLocation = false;

            final ConstraintLayout layout = spotlightView.findViewById(R.id.container);
            switch (largest) {
                case ABOVE_SPOTLIGHT:
                    spotlightView.getViewTreeObserver()
                            .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                                @Override
                                public void onGlobalLayout() {
                                    layout.setY(point.y - radius - 100 - layout.getHeight());
                                }
                            });
                    break;
                case BELOW_SPOTLIGHT:
                    layout.setY((int) (point.y + radius + 8));
                    break;
            }
        }
    }
}
