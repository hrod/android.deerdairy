package com.hrod.deerdiary.spotlight;

import android.animation.Animator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.AttrRes;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Spotlight View
 **/
class SpotlightView extends FrameLayout {

    private final Paint paint = new Paint();
    private final Paint spotPaint = new Paint();
    private PointF point = new PointF();
    private float radius = 0f;
    private ValueAnimator animator;
    private OnSpotlightStateChangedListener listener;
    private int overlayColor;


    public SpotlightView(@NonNull Context context) {
        super(context, null);
        init();
    }

    public SpotlightView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs, 0);
        init();
    }

    public SpotlightView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void setOnSpotlightStateChangedListener(OnSpotlightStateChangedListener l) {
        this.listener = l;
    }

    public void setOverlayColor(@ColorInt int overlayColor) {
        this.overlayColor = overlayColor;
    }

    private void init() {
        bringToFront();
        setWillNotDraw(false);
        setLayerType(View.LAYER_TYPE_HARDWARE, null);
        spotPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    PointF touchPosition = new PointF();
                    touchPosition.set(event.getX(), event.getY());

                    if (animator != null && !animator.isRunning() && (float) animator.getAnimatedValue() > 0) {
                        if (listener != null) {
                            if(checkInsideRect(point, touchPosition, radius))listener.onTargetClickedInside();
                            else listener.onTargetClickedOutside();
                        }
                    }
                }
                return false;
            }
        });
    }

    /**
     * set dim background with trim circle!!
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        paint.setColor(overlayColor);
        canvas.drawRect(0, 0, canvas.getWidth(), canvas.getHeight(), paint);
        if (animator != null) {
            canvas.drawCircle(point.x, point.y, (float) animator.getAnimatedValue(), spotPaint);
        }
    }

    /**
     * draw circle with animation!!
     *
     * @param x         init position x where the circle is showing up
     * @param y         init position y where the circle is showing up
     * @param radius    radius of the circle
     * @param duration  duration of the animation
     * @param animation type of the animation
     */
    void turnUp(float x, float y, float radius, long duration, TimeInterpolator animation) {
        this.point.set(x, y);
        this.radius = radius;
        animator = ValueAnimator.ofFloat(0f, radius);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                SpotlightView.this.invalidate();
            }
        });
        animator.setInterpolator(animation);
        animator.setDuration(duration);
        animator.start();
    }

    /**
     * starts an animation to close the circle!!
     *
     * @param radius    radius of the circle
     * @param duration  duration of the animation
     * @param animation type of the animation
     */
    void turnDown(float radius, long duration, TimeInterpolator animation) {
        animator = ValueAnimator.ofFloat(radius, 0f);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                SpotlightView.this.invalidate();
            }
        });
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (listener != null) listener.onTargetClosed();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animator.setInterpolator(animation);
        animator.setDuration(duration);
        animator.start();
    }

    private boolean checkInsideRect(PointF point, PointF touchPosition, float radius){
        float tempX = point.x - touchPosition.x;
        float tempY = point.y - touchPosition.y;
        int distance = (int) Math.sqrt(tempX*tempX + tempY*tempY);
        if(distance <= radius) return true;
        return false;
    }

    interface OnSpotlightStateChangedListener {
        void onTargetClosed();
        void onTargetClickedOutside();
        void onTargetClickedInside();
    }
}
