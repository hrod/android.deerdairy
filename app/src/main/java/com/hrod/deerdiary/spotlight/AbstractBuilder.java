package com.hrod.deerdiary.spotlight;

import android.app.Activity;
import android.graphics.PointF;
import android.view.View;

import androidx.annotation.NonNull;

import java.lang.ref.WeakReference;

/**
 * Position Target
 **/
abstract class AbstractBuilder<T extends AbstractBuilder<T, S>, S extends Target> {

    private WeakReference<Activity> contextWeakReference;

    OnTargetStateChangedListener listener;
    float startX = 0f;
    float startY = 0f;
    float radius = 100f;
    boolean isClosedOnTouchedOutside = true;
    boolean isClosedOnTouchedInside = true;

    /**
     * return the builder itself
     */
    protected abstract T self();

    /**
     * return the built {@link Target}
     */
    protected abstract S build();

    /**
     * Return context weak reference
     *
     * @return the activity
     */
    Activity getContext() {
        return contextWeakReference.get();
    }

    /**
     * Constructor
     */
    AbstractBuilder(@NonNull Activity context) {
        contextWeakReference = new WeakReference<>(context);
    }

    /**
     * Sets the initial position of target
     *
     * @param y starting position of y where spotlight reveals
     * @param x starting position of x where spotlight reveals
     * @return This Builder
     */
    public T setPoint(float x, float y) {
        this.startX = x;
        this.startY = y;
        return self();
    }

    /**
     * Sets the initial position of target
     *
     * @param point starting position where spotlight reveals
     * @return This Builder
     */
    public T setPoint(@NonNull PointF point) {
        return setPoint(point.x, point.y);
    }

    /**
     * Sets the initial position of target
     * Make sure the view already has a fixed position
     *
     * @param view starting position where spotlight reveals
     * @return This Builder
     */
    public T setPoint(View view) {
        if(view == null){
            return setPoint(-1,-1);
        }
        int[] viewLocation = new int[2];
        view.getLocationOnScreen(viewLocation);
        
        int[] location = new int[2];
        view.getLocationInWindow(location);
        int x = location[0] + view.getWidth() / 2;
        int y = location[1] + view.getHeight() / 2;
        return setPoint(viewLocation[0] + x, viewLocation[1] + y);
    }

    public T setPoint(View view, float percentX, float percentY){
        if(view == null) throw new NullPointerException("setPointPercent view is null");
        if(percentX > 1 || percentX < 0) throw new IllegalArgumentException("position x must be greater than 0, smaller than 1");
        if(percentY > 1 || percentY < 0) throw new IllegalArgumentException("position x must be greater than 0, smaller than 1");
    
        int[] viewLocation = new int[2];
        view.getLocationOnScreen(viewLocation);
        
        int largeX = view.getWidth();
        int largeY = view.getHeight();
        float[] location = new float[2];
        location[0] = largeX*percentX;
        location[1] = largeY*percentY;

        return setPoint(viewLocation[0] + location[0], viewLocation[1] + location[1]);
    }

    /**
     * Sets the radius of target
     *
     * @param radius radius of target
     * @return This Builder
     */
    public T setRadius(float radius) {
        if(startX == -1 && startY == -1){
            this.radius = 0;
        }
        if(radius == 0f){
            this.radius = 0.1f;
        }
        if (radius < 0) {
            throw new IllegalArgumentException("radius must be greater than 0");
        }
        this.radius = radius;
        return self();
    }

    /**
     * Sets Target state changed Listener to\ target
     *
     * @param listener OnTargetStateChangedListener of target
     * @return This Builder
     */
    public T setOnSpotlightStartedListener(@NonNull final OnTargetStateChangedListener<S> listener) {
        this.listener = listener;
        return self();
    }

    public T setClosedOnTouchedOutside(boolean isClosedOnTouchedOutside){
        this.isClosedOnTouchedOutside = isClosedOnTouchedOutside;
        return self();
    }

    public T setClosedOnTouchedInside(boolean isClosedOnTouchedInside) {
        this.isClosedOnTouchedInside = isClosedOnTouchedInside;
        return self();
    }
}
