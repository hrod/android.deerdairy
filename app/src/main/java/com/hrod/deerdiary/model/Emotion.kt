package com.hrod.deerdiary.model

data class Emotion(
    var img: String,
    var state: String
)
