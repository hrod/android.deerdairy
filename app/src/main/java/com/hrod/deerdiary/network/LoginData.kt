package com.hrod.deerdiary.network

data class LoginData(
    val email: String,
    val password: String
)