package com.hrod.deerdiary.network

data class MemberDiary(
    val diaryId: Int,

    val groupId: Int,

    val memberId: Int,

    val title: String,

    val content: String,

    val shared: Int,

    val deleted: Int,

    val createDatetime: String,

    val updateDatetime: String,

    val thumbup: Int,

    val analysis: String
)
