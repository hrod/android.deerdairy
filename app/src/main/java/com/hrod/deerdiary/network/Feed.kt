package com.hrod.deerdiary.network

data class Feed(
    var diaryId : Int? =null,
    var groupId : Int? =null,
    var memberId : Int? =null,
    var title : String? =null,
    var content : String? =null,
    var shared : Int? =null,
    var deleted : Int? =null,
    var createDatetime : String? =null,
    var updateDatetime : String? =null,
    var thumbup : Int? =null,
    var analysis : String? =null,
)

