package com.hrod.deerdiary.network

import retrofit2.Call
import retrofit2.http.*

interface API {
    @POST("/api/v1/auth/login")
    fun login(@Body jsonparams: LoginData): Call<String>

    @GET("/api/v1/diary/feed")
    fun getFeed(): Call<List<Feed>>
}
