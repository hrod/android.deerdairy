package com.hrod.deerdiary.ui.summary

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.applandeo.materialcalendarview.CalendarView
import com.hrod.deerdiary.R
import com.hrod.deerdiary.adpater.EmotionAdapter
import com.hrod.deerdiary.databinding.FragmentSummaryBinding
import com.hrod.deerdiary.model.Emotion
import java.util.*
import kotlin.collections.ArrayList

class SummaryFragment : Fragment() {

    private lateinit var summaryViewModel: SummaryViewModel
    private var _binding: FragmentSummaryBinding? = null
    private val binding get() = _binding!!

    private lateinit var emotion: ListView
    private lateinit var emotionCalendar: CalendarView

    private val emotionList: ArrayList<Emotion> = arrayListOf(
        Emotion("happy", "행복 - 20%"),
        Emotion("sad", "슬픔 - 50%"),
        Emotion("heart", "사랑 - 1%")
    )

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        summaryViewModel = ViewModelProvider(this).get(SummaryViewModel::class.java)
        _binding = FragmentSummaryBinding.inflate(inflater, container, false)
        val root: View = binding.root

        initView()

        emotion.adapter = EmotionAdapter(context, emotionList)
        setListViewHeight(emotion)

        /*emotionCalendar.setOnDateChangeListener { _, year, month, day ->
            val calendar =  Calendar.getInstance()
            calendar.set(year, month, 11)
            calendar.set(year, month, day)
            calendar.set(year, month, 22)
        }*/

        return root
    }

    private fun initView(){
        emotion = binding.emotionListview
        emotionCalendar = binding.emotionCalendar
    }

    private fun setListViewHeight(listview: ListView){
        val listAdapter = listview.adapter ?: return

        var totalHeight = 0
        for(i in 0 until listAdapter.count){
            val item = listAdapter.getView(i, null, listview)
            item.measure(0, 0)
            totalHeight += item.measuredHeight
        }

        val params = listview.layoutParams as ViewGroup.LayoutParams
        params.height = totalHeight + (listview.dividerHeight * (listAdapter.count - 1))
        listview.layoutParams = params
        listview.requestLayout()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}