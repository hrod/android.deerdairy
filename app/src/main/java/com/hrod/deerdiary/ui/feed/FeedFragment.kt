package com.hrod.deerdiary.ui.feed

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.hrod.deerdiary.databinding.FragmentFeedBinding
import com.hrod.deerdiary.network.Feed
import com.hrod.deerdiary.network.RetrofitClass
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FeedFragment : Fragment() {

    private lateinit var feedViewModel: FeedViewModel
    private var _binding: FragmentFeedBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        feedViewModel =
                ViewModelProvider(this).get(FeedViewModel::class.java)

        _binding = FragmentFeedBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val textView: TextView = binding.textHome
        feedViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })

        RetrofitClass.api.getFeed().enqueue(object : Callback<List<Feed>> {
            override fun onResponse(call: Call<List<Feed>>, response: Response<List<Feed>>) {
                Log.d("Feed response : ", response.toString())
                if (response.code() == 200) {
                    Toast.makeText(getContext(), "피드 불러오기 성공", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(getContext(), "fail", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<List<Feed>>, t: Throwable) {
                Log.d("Feed response : ", "Fail")
                Toast.makeText(getContext(), "fail", Toast.LENGTH_SHORT).show()
            }
        })

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}