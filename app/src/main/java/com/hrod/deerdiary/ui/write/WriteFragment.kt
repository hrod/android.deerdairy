package com.hrod.deerdiary.ui.write

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.hrod.deerdiary.LoginActivity
import com.hrod.deerdiary.databinding.FragmentWriteBinding

class WriteFragment : Fragment() {

    private lateinit var writeViewModel: WriteViewModel
    private var _binding: FragmentWriteBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        writeViewModel = ViewModelProvider(this).get(WriteViewModel::class.java)
        _binding = FragmentWriteBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val textView: TextView = binding.textWrite
        writeViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })

        openWriteDiary()

        return root
    }

    private fun openWriteDiary(){
        startActivity(Intent(context, WriteActivity::class.java))
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}