package com.hrod.deerdiary.ui.write

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.hrod.deerdiary.R
import com.hrod.deerdiary.adpater.SelectDiaryAdapter
import com.hrod.deerdiary.databinding.ActivityWriteBinding

class WriteActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var binding: ActivityWriteBinding
    private lateinit var writeCancel: ImageView
    private lateinit var writeDiarySelect: TextView
    private lateinit var writeDate: TextView
    private lateinit var writeDesc: EditText
    private lateinit var writeDescSize: TextView
    private lateinit var calendarLayout: ConstraintLayout
    private lateinit var calendar: CalendarView
    private lateinit var selectDiaryLayout: ConstraintLayout
    private lateinit var selectDiary: ListView
    private lateinit var selectedDiary: TextView

    private lateinit var adapter:SelectDiaryAdapter

    private val diaryNameList: ArrayList<String> = arrayListOf(
        "다이어리1"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_write)

        binding = ActivityWriteBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initView()
        initOnClickListener()

        calendar.setOnDateChangeListener { _, year, month, day ->
            writeDate.text = "${year}년 ${month + 1}월 ${day}일"
            calendarLayout.visibility = View.GONE
        }

        writeDesc.addTextChangedListener(object: TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, size: Int, p2: Int, p3: Int) {
                writeDescSize.text = "${writeDesc.text.length}"
            }
            override fun afterTextChanged(editText: Editable?) {}
        })
    }

    private fun initView(){
        writeCancel = binding.btnWriteCancel
        writeDiarySelect = binding.writeDiarySelect
        writeDate = binding.writeDateSelect
        writeDesc = binding.writeDesc
        writeDescSize = binding.writeTotalLength
        calendarLayout = binding.includeCalendarView.calendarLayout
        calendar = binding.includeCalendarView.calendarView
        selectDiaryLayout = binding.includeSelectDiary.selectDiaryLayout
        selectDiary = binding.includeSelectDiary.selectDiaryListview
        selectedDiary = binding.includeSelectDiary.selectedDiary

        adapter = SelectDiaryAdapter(this, diaryNameList)
    }

    private fun initOnClickListener(){
        writeCancel.setOnClickListener(this)
        writeDiarySelect.setOnClickListener(this)
        writeDate.setOnClickListener(this)
        selectedDiary.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when(view?.id){
            R.id.btn_write_cancel -> {
                val intent = Intent()
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
            R.id.write_diary_select -> {
                selectDiaryLayout.visibility = View.VISIBLE
                selectDiary.adapter = adapter
            }
            R.id.write_date_select -> {
                val inputMethod = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethod.hideSoftInputFromWindow(writeDate.windowToken, 0)

                calendarLayout.visibility = View.VISIBLE
            }
            R.id.selected_diary -> {
                selectDiaryLayout.visibility = View.GONE
                writeDiarySelect.text = adapter.getItem(adapter.selectedPosition)
            }
        }
    }
}