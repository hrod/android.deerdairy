package com.hrod.deerdiary.ui.my

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.hrod.deerdiary.LoginActivity
import com.hrod.deerdiary.databinding.FragmentMyBinding
import com.hrod.deerdiary.network.LoginData
import com.hrod.deerdiary.network.RetrofitClass
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MyFragment : Fragment() {

    private lateinit var writeViewModel: MyViewModel
    private var _binding: FragmentMyBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        writeViewModel =ViewModelProvider(this).get(MyViewModel::class.java)
        _binding = FragmentMyBinding.inflate(inflater, container, false)
        val root: View = binding.root

        openLogin()

        //val textView: TextView = binding.text
        writeViewModel.text.observe(viewLifecycleOwner, Observer {
            // textView.text = it
        })
        return root
    }

    private fun openLogin(){
        startActivity(Intent(context, LoginActivity::class.java))
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}