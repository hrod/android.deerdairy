package com.hrod.deerdiary

import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import com.hrod.deerdiary.network.LoginData
import com.hrod.deerdiary.network.RetrofitClass
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : BaseActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_login)

        val btn_event = findViewById<Button>(R.id.login_button)
        val _id = findViewById<EditText>(R.id.edit_text_id)
        val _pw = findViewById<EditText>(R.id.edit_text_password)

        btn_event.setOnClickListener {
            RetrofitClass.api.login(LoginData(_id.text.toString(),_pw.text.toString())).enqueue(object :
                Callback<String> {
                override fun onResponse(call: Call<String>, response: Response<String>) {
                    Log.d("Login response : ", response.toString())
                }

                override fun onFailure(call: Call<String>, t: Throwable) {
                    Log.d("Login response : ", "Fail")
                }
            })
        }
    }
}